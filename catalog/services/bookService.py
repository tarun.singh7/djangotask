from core.BaseService import BaseService
from catalog.models import Book, Author, BookInstance, Genere, Language
from django.views import generic
import json
from catalog.serealizer.catalogSerializer import BookSerializer


class BookService(BaseService):
    def book_detail_view(key):
        book_with_id = Book.objects.filter(isbn=key).values()
        # breakpoint()
        if book_with_id:
            return BaseService.get_200_response(data=book_with_id)

        return BaseService.get_404_response(errors="requested data not found")

    def index():
        num_books = Book.objects.all().count()
        num_instances = BookInstance.objects.all().count()
        num_instances_available = BookInstance.objects.filter(
            status__exact='a').count()
        num_author = Author.objects.count()

        context = {
            'num_books': num_books,
            'num_instances': num_instances,
            'num_instances_available': num_instances_available,
            'num_author': num_author,
        }

        if context:
            return BaseService.get_200_response(data=context)

        return BaseService.get_404_response(errors="requested page not found")

    def addNewBook(request):
        # breakpoint()
        pyobj = request.data
        # pyobj = json.loads(jsonobj)

        serializer = BookSerializer(data=pyobj)
        # breakpoint()
        if serializer.is_valid():
            serializer.save()
            return BaseService.get_201_response(data=pyobj)

        return BaseService.get_400_response(errors=serializer.errors)
