from django.contrib import admin
from .models import Author, Genere, Book, BookInstance, Language

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genere)
admin.site.register(BookInstance)
admin.site.register(Language)

