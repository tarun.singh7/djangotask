import uuid

from django.db import models
from django.urls import reverse

# Create your models here.


class Genere(models.Model):
    name = models.CharField(
        max_length=200, help_text='enter a book genere (e.g. Science Fiction)')

    def __str__(self):
        return self.name


class Language(models.Model):
    language_name = (
        ('HIN', 'Hindi'),
        ('ENG', 'English'),
        ('FAR', 'Farsi'),
    )
    language_choice = models.CharField(
        max_length=30, choices=language_name, default='English', help_text="please select the language of Book")

    def __str__(self):
        return self.language_choice


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True)
    summary = models.TextField(
        max_length=1000, help_text='enter a breif discription of the book')
    isbn = models.CharField('ISBN', max_length=13,
                            unique=True, help_text="13 character ISBN Number")
    genere = models.ManyToManyField(
        Genere, help_text="select a genere for this book")
    language = models.ForeignKey(
        Language, on_delete=models.RESTRICT, null=True)

    def get_absolute_url(self):
        return reverse('book-detail-url', args=[str(self.isbn)])


class BookInstance(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          help_text="unique id for this particular book accross whole library")
    book = models.ForeignKey(Book, on_delete=models.RESTRICT, null=True)
    imprint = models.DateField(null=True, blank=True)

    LOAN_STATUS = (
        ('m', 'Maintenace'),
        ('o', 'On Loan'),
        ('a', 'Available'),
        ('r', 'Reserved'),
    )

    status = models.CharField(
        max_length=1,
        choices=LOAN_STATUS,
        blank=True,
        default='m',
        help_text='Book availabilty',
    )

    def __str__(self):
        return f'{self.id}({self.book.title})'


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)

    def get_absolute_url(self):
        return reverse('author-detail', args=[str(self.id)])

    def __str__(self):
        return f'{self.last_name} , {self.first_name}'
