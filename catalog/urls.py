from django.urls import path
from django.views import View
from . import views

urlpatterns = [
    path('book/<pk>', views.BookDetailView.as_view(), name='book-detail-url'),
    path('books/', views.BookListView.as_view(), name='books-url'),
    path('', views.IndexView.as_view(), name='index-url')
]
