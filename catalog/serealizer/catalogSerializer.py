from rest_framework import serializers
from catalog.models import Book,Author,BookInstance,Genere,Language

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['title', 'author', 'summary', 'isbn', 'genere', 'language']