import time

number_of_request = 0
request_time = {}


class CountRequest:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        global number_of_request
        number_of_request += 1

        response = self.get_response(request)
        return response


class RequestLatency:
    global request_time

    def __init__(self, get_response):
        self.get_response = get_response
        self.start_time = 0

    def __call__(self, request):
        self.start_time = time.time()

        response = self.get_response(request)

        time_taken = time.time() - self.start_time
        request_time[number_of_request] = time_taken
        print("The coresponding request id with time are listed here : \n", request_time)
        return response
