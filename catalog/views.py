from django.shortcuts import render
from django.http import HttpResponse
from catalog.models import Book, Author, BookInstance, Genere, Language
from django.views import generic

from catalog.services.bookService import BookService
from rest_framework.views import APIView
from rest_framework.response import Response

######## CLASS BASED IMPLEMENTATION OF VIEWS USING LIST VIEW ##############


class BookListView(generic.ListView):
    model = Book
    context_object_name = 'book_list'
    template_name = 'booklist.html'

############ SERVICE CALLED IN CLASS BASED IMPLEMENTATION ####################


class BookDetailView(APIView):
    def get(self, request, pk):
        book = BookService.book_detail_view(pk)
        return Response(book, status=book['code'])


class IndexView(APIView):
    def get(self, request, format=None):
        count_book = BookService.index()
        # breakpoint()
        return Response(count_book, status=count_book['code'])

    def post(self, request):
        added_book_response = BookService.addNewBook(request)
        # breakpoint()
        return Response(added_book_response, status=added_book_response['code'])


# use this type of example to add a new book
# {
#   "title" : "ninth book",
#   "summary" : "this is my ninth book",
#   "author" : 1,
#   "isbn" : "1234567890AAI",
#   "genere" : [1],
#   "language" : 1
# }
